/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2017-2018 ForgeRock AS.
 */


package com.forgerock.dss.nodes;

import static org.forgerock.openam.auth.node.api.Action.send;

import java.net.URISyntaxException;

import javax.inject.Inject;
import com.sun.identity.authentication.spi.RedirectCallback;
import java.net.URI;
import java.util.List;

import org.apache.http.client.utils.URIBuilder;
import org.forgerock.openam.annotations.sm.Attribute;
import org.forgerock.openam.auth.node.api.*;
import org.forgerock.openam.core.realms.Realm;
import org.forgerock.openam.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.assistedinject.Assisted;

/**
 * A node that checks to see if zero-page login headers have specified username and whether that username is in a group
 * permitted to use zero-page login headers.
 */
@Node.Metadata(outcomeProvider  = SingleOutcomeNode.OutcomeProvider.class,
               configClass      = RedirectURLNode.Config.class)
public class RedirectURLNode extends AbstractDecisionNode {

    private final Config config;
    private final Realm realm;
    private final static String DEBUG_FILE = "RedirectURLNode";
    private final Logger logger = LoggerFactory.getLogger(RedirectURLNode.class);

    /**
     * Configuration for the node.
     */
    public interface Config {
        @Attribute(order = 100)
        default String redirectURL() {
            return "http://www.example.com";
        }
    }

    /**
     * Create the node using Guice injection. Just-in-time bindings can be used to obtain instances of other classes
     * from the plugin.
     *
     * @param config The service config.
     * @param realm The realm the node is in.
     * @throws NodeProcessException If the configuration was not valid.
     */
    @Inject
    public RedirectURLNode(@Assisted Config config, @Assisted Realm realm) throws NodeProcessException {
        this.config = config;
        this.realm = realm;
    }

    @Override
    public Action process(TreeContext context) throws NodeProcessException {
        RedirectCallback redirectCallback;
        URI uri = null;
        URI redirectURI = URI.create(config.redirectURL());
        // String gotoParam = String.valueOf(context.request.parameters.get("goto"));
        List<String> gotoParams = context.request.parameters.get("goto");
        String gotoParam = "";
        if (gotoParams != null) {
            gotoParam = gotoParams.get(0);
        }
        URIBuilder urlToRedirect = new URIBuilder(redirectURI);
        if (!StringUtils.isEmpty(gotoParam)) {
            logger.info("***Goto param : " + gotoParam);
            urlToRedirect.addParameter("goto", gotoParam);
        }
        try {
            uri = urlToRedirect.build();
        } catch (final URISyntaxException e) {
            e.printStackTrace();
        }
        redirectCallback = new RedirectCallback(uri.toString(), null, "GET");
        return send(redirectCallback).build();    
    }
}
